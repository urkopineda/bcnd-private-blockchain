// Imports
const level = require('./levelSandbox');

/* ===== SHA256 with Crypto-js ===============================
|  Learn more: Crypto-js: https://github.com/brix/crypto-js  |
|  =========================================================*/
const SHA256 = require('crypto-js/sha256');


/* ===== Block Class ==============================
|  Class with a constructor for block 			   |
|  ===============================================*/
class Block {
  constructor(data) {
    this.hash = '';
    this.height = 0;
    this.body = data;
    this.time = 0;
    this.previousBlockHash = '';
  }
}

/* ===== Blockchain Class ==========================
|  Class with a constructor for new blockchain 		|
|  ================================================*/
class Blockchain {
  constructor() {
    this.chain = level;
    this.processingGenesis = false;
  }

  // Add new block
  async addBlock(newBlock) {
    let actualHeight = await this.getBlockHeight();
    // Check if there is a genesis block, if not, add it
    if (actualHeight === -1 && !this.processingGenesis) {
      this.processingGenesis = true;
      await this.addBlock(new Block('First block in the chain - Genesis block'));
      actualHeight = await this.getBlockHeight();
      this.processingGenesis = false;
    }
    // Block height
    newBlock.height = (actualHeight === -1) ? 0 : actualHeight + 1;
    // UTC timestamp
    newBlock.time = new Date().getTime().toString().slice(0, -3);
    // Previous block hash
    if (newBlock.height > 0) {
      let previousBlock = await this.getBlock(newBlock.height - 1);
      newBlock.previousBlockHash = previousBlock.hash;
    }
    // Block hash with SHA256 using newBlock and converting to a string
    newBlock.hash = SHA256(JSON.stringify(newBlock)).toString();
    // Adding block object to chain
    await this.chain.addKey(newBlock.height, JSON.stringify(newBlock).toString());
  }

  // Get block height
  async getBlockHeight() {
    return await this.chain.dataNumber();
  }

  // Get block
  async getBlock(blockHeight) {
    // Return object as a single string
    return JSON.parse(await this.chain.get(blockHeight));
  }

  // Validate block
  async validateBlock(blockHeight) {
    // Get block object
    let block = await this.getBlock(blockHeight);
    // Get block hash
    let blockHash = block.hash;
    // Remove block hash to test block integrity
    block.hash = '';
    // Generate block hash
    let validBlockHash = SHA256(JSON.stringify(block)).toString();
    // Compare
    if (blockHash === validBlockHash) {
      return true;
    } else {
      console.warn(`Block #${blockHeight} invalid hash:\n${blockHash}<>${validBlockHash}`);
      return false;
    }
  }

  // Validate blockchain
  async validateChain() {
		let chainLength = await this.getBlockHeight();
    let errorLog = [];
    for (let i = 0; i < chainLength; i++) {
      // Validate block
      if (!await this.validateBlock(i)) errorLog.push(i);
      // Compare blocks hash link
      let block = await this.getBlock(i);
      let previousBlock = await this.getBlock(i + 1);
      let blockHash = block.hash;
      let previousHash = previousBlock.previousBlockHash;
      if (blockHash !== previousHash) {
        errorLog.push(i);
      }
    }
    if (errorLog.length > 0) {
      console.warn(`Block errors = ${errorLog.length}`);
      console.warn(`Block: ${errorLog}`);
			return false;
    } else {
			return true;
    }
  }
}

// Exports
module.exports.Block = Block;
module.exports.Blockchain = Blockchain;
