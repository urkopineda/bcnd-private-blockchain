const chai = require('chai');
const expect = chai.expect;

const LevelSandbox = require('../src/levelSandbox');

const BLOCKS_TO_ADD = 10;

describe('Testing LevelDB sandbox', () => {
	let value = 'Testing data number ';

	describe('add data', () => {
		it(`should add ${BLOCKS_TO_ADD} elements`, async () => {
      for (let i = 0; i <= BLOCKS_TO_ADD; i++) {
				expect(await LevelSandbox.add(`Testing data number ${i}`)).to.equal(i);
      }
			expect(await LevelSandbox.get(BLOCKS_TO_ADD)).to.equal(`Testing data number ${BLOCKS_TO_ADD}`);
		});
	});

	describe('get data length', () => {
		it(`should get correct data lenght`, async () => {
			expect(await LevelSandbox.dataNumber()).to.equal(BLOCKS_TO_ADD);
		});
	});

	describe('update', () => {
		it(`should update an element`, async () => {
			let key = 0;
			let data = 'New content';
			expect(await LevelSandbox.update(key, data)).to.equal(true);
			expect(await LevelSandbox.get(key)).to.equal(data);
		});
	});

	describe('delete all data', () => {
		it(`should delete all data`, async () => {
			expect(await LevelSandbox.deleteAll()).to.equal(true);
		});
	});
});
